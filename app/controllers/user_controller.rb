class UserController < ApplicationController

	 before_action :authenticate_user!
	def index
		@users= User.all
	end

	def show
		@users = User.find(params[:id])
	end


	def destroy

		#usuarios=current_user.active_relationships.find_by (params[:id])
		#usuarios=current_user.active_relationships.find_by (params[:followed_id])
		usuarios = User.find(params[:id])
		usuarios.unfollow(current_user)
		current_user.unfollow(usuarios)


		respond_to do |format|
			format.html { redirect_to followers_user_url, notice: 'Se ha eliminado esa relación' }
			format.json { head :no_content }
		end
	end
 def following
    @title = "Friends"
    @user  = User.find(params[:id])
    @users = @user.following
    render 'show_follow'

  end

  def followers
    @title = "Requests"
    @user  = User.find(params[:id])
    @users = @user.followers
    render 'show_follow'
  end

end
