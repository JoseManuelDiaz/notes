json.extract! notum, :id, :title, :body, :created_at, :updated_at
json.url notum_url(notum, format: :json)
