class AddAttachmentImageToNota < ActiveRecord::Migration[5.1]
  def self.up
    change_table :nota do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :nota, :image
  end
end
